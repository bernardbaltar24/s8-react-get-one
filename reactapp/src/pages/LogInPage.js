import React from 'react';
import {Container , Row, Col} from 'reactstrap';
import LoginForm from '../forms/LoginForm';
import { Link, Redirect } from 'react-router-dom';

const LoginPage = (props) => {

	// if(props.token) return <Redirect to="/profile" />

	return(
		<Container>
			<Row>
				<Col>
				  <h1>Login Page</h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<LoginForm/>
				</Col>
			</Row>
		</Container>

		)
}

export default LoginPage;